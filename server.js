const express = require('express');
const app = express();
const axios = require('axios')
const bodyParser = require('body-parser');

const TEAMS = {
    mrui: {
        numApprovers: 2,
        approvers: [
            2134204, // @xzhu1
            2286932, // @dprgarner
            2247914, // @aocallaghan
            2306092, // @prnechifor
            2360300, // @alehman
            2575603, // @jsimpson91
            2294543, // @steveandrewarcher
        ],
    },
    sherpa: {
        numApprovers: 2,
        approvers: [
            2249217, // Andrew
            2248043, // Toby
            2248493, // James
            2292516, // Hintsa
            2289239, // Chris
        ],
        gChatUrl: "https://chat.googleapis.com/v1/spaces/AAAApqMce0k/messages?key=AIzaSyDdI0hCZtE6vySjMm-WEfRq3CPzqKqqsHI&token=CdfhXbnfn7JKR6MeAVRGofbc9H1RGDYRWnvgZlJ7ltQ%3D",
        gChatName: {
            "2249217": "101134780097388222612", // Andrew
            "2248043": "110010283424598632176", // Toby
            "2294543": "110856836894788371629", // Steve
            "2248493": "103268863336360748324", // James
            "2292516": "110398622303723974719", // Hintsa
            "2289239": "111758968061959223813", // Chris
        }
    }
}

function getApprovers(teamKey, mrId, author) {
    const team = TEAMS[teamKey];
    const startIdx = Math.round(((mrId % (team.approvers.length / team.numApprovers)) - 1) * team.numApprovers);
    approvers = [];
    let loopLength = team.numApprovers
    for (i=0; i<loopLength; i++) {
        const approverIdx = startIdx + i >= 0 ? startIdx + i : team.approvers.length + (startIdx + i);
        if(team.approvers[approverIdx] !== author) {
            approvers.push(team.approvers[approverIdx]);
        } else {
            loopLength++;
        }
    }
    return approvers;
}

app.use(bodyParser.json());

app.post('/', function(req, res) {
    const event = req.body;
    const team = req.header('X-Gitlab-Token');

    if (event.object_attributes.action === "open") {
        // figure out assignees
        const mrId = event.object_attributes.iid;
        const approvers = getApprovers(team, mrId, event.object_attributes.author_id);
        // send assignees to gitlab api
        const url = 'https://gitlab.com/api/v4/projects/'+event.project.id+'/merge_requests/'+mrId+'?private_token=rTFCStVoAxUzukBusDpt';
        axios.put(
            url,
            {
                id: event.project.id,
                merge_request_iid: mrId,
                assignee_ids: approvers,
            },
        ).then((gitlabRes) => {
            if(TEAMS[team]['gChatUrl']) {
                let message = '';
                approvers.forEach(approver => {
                    message += `<users/${TEAMS[team]['gChatName'][approver.toString()]}>, `
                });
                message += `you've been assigned to this MR: ${event.object_attributes.url}`;
                axios.post(
                    TEAMS[team]['gChatUrl'],
                    {
                        'text': message
                    },
                )
            }
            res.sendStatus(200);
        }).catch((err) => {
            res.sendStatus(400);
        });
    } else {
        res.sendStatus(200);
    }
})

app.listen(process.env.PORT || 4000, function(){
    console.log('Your node js server is running');
});